import {sendRequest} from './client.js';
const URL_GET_IP = 'https://api.ipify.org/?format=json'
const URL_LOCATION_BY_IP = 'http://ip-api.com/json/'

const resultElement = document.querySelector('#result');
async function searchByIp() {
    const ip = await sendRequest(URL_GET_IP, "GET").then((data) => {
        return data.ip;
    });

    const location = await sendRequest(`${URL_LOCATION_BY_IP}${ip}`, "GET");

    resultElement.innerHTML = `
        <p>IP: ${location.query}</p>
        <p>Country: ${location.country}</p>
        <p>City: ${location.city}</p>
        <p>Region: ${location.regionName}</p>
        <p>ISP: ${location.isp}</p>
    `
}

document.querySelector('#search').addEventListener('click', searchByIp);
